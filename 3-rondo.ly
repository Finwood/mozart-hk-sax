%vim: nocursorline :
% KV 495.3: Rondo

SaxR = {
  \transpose c f {
    \relative c'' {
      \compressFullBarRests
      \transposition bes,

      \clef "treble" \key c \major \time 6/8

      \partial 8 g8 |
      c8\mf c c c c c | b( d g) g( f d) | c c c c b c | d g, g g4 g8 | c c c c c c | b( d g) g( f d) | e g c, d c d | c c, c c4 r8 | R2.*7 | r4 r r8 g'8 |
      % ------------- Takt 17
      c4. e4. | g4.~g4 e8 | f d c b a b | c4 e8 g,4 g8 | c4. e4. | g4.~g8 e8 c | f d b g a b | c4 r8 r4 r8 | r r e\p e f e | e4 r8 r4 r8 | r r d d e d | d4 d8 d e fis | g4 e8 b4 cis8 | d4 r8 r4 r8 | r2. |
      % ------------- Takt 32
      d4.~d8 cis d | e4 e8 e fis e | d4 d8 d cis d | e4 e8 e fis e | d4 r8 r4 r8 | r2. | r8 r d\mf b4 g'8 | e4 c8 g4 g'8 | d4 b8 g4  g'8 | g fis e d e fis | g4 r8 r4 r8 | g,4. c4 e8 | g,4. b4 d8 | c4 a8 d4 c8 | b4 r8 r4 r8 | r2. |
      % ------------- Takt 48
      r8 c,8\f e g c e | g4.~g8 fis e | d g fis e d c | b4. a4. | g4 r8 r4 r8 | r2. | r8 c,8\p e g c e | g4.~g8 fis e | d4.~d8 e cis | d4.~d8 e cis | d e cis d e cis | d2. | g,4 r8 r4 r8 | r2. | g'2.\< | fis2. | f2. | e4 d8 c d e | f4 r8 d e f | g4 r8 g,8 a b |
      % ------------- Takt 68
      c\mf c c c c c | b( d g) g( f d) | c c c c b c | d g, g g4 g8 | c c c c c c | b( d g) g( f d) | e g c, d c d | c c, c c4 r8 | R2.*7 r2.\fermata |
      % ------------- Takt 84
      e'4.\pp e4. | e4.~e8 c a | f'4 f8 f4 f8 | f4( e8) e4 r8 | c4. b4( gis8) | e'4.~e4 d8 |c a b c c d | e4 r8 r4 r8 | e4. e4. | e4.~e8 c a | f'4 f8 f4 f8 | f2. | e4 g8 bes,4. | a4 c8 es4. | d4 g8 f4 e8 | f4 r8 r4 r8 | r r c c d c | c4 r8 r4 r8 | r r d d es d | d4 r8 r4 r8 | r r e e f e | e4 e8 e dis e | f4.~f4 e8 | dis r r e r r | fis r r dis r r | e4 e8 e dis e | f4.~f4 e8 | dis r r e r r | fis r r dis r r | e4 r8 r4 r8 | r2. | r2. | dis2.\<( | e4.) r4 r8 | r2. | r2. | cis2.( | d4) r8 r4 r8 | r2. | r2. | b2. |
      % ------------- Takt 125
      c8\mf c c c c c | b( d g) g( f d) | c c c c b c | d g, g g4 g8 | c c c c c c | b( d g) g( f d) | e g c, d c d | c c, c c4 r8 | R2.*7 | r4 r r8 g'8 |
      % ------------- Takt 141
      c4. e4. | g4.~g4 e8 | f d c b a b | c4 e8 g,4 g8 | c4. es4. | g2. | c,4. es4. | as2. | c,4. es4. | fis2. | g4 r8  r r d | d4 d8 d4 d8 | d( g) d d d d | d d d d d d | g,4 r8 r4 r8 | r2. | r8 r c e4 g8 | a,4 c8 f4 a8 | g4 e8 c4 e8 | f8 d c b a b | c4. r8 r8. c,16 | c4.~c4~c16 c16 | c4.~c4~c16 c16 | c4 c16 c c4 c16 c | c4 r8 r4 r8 | r2. | r8 c e g c e | g2. | fis2. | f2. | e8 d e f g a | c,4. \appoggiatura e8 d4 c8 | c4 r8 r4 r8 | r2. | r8 c, e g c e | g2. | fis2. | f2. | es4 r8 r4 r8 | des4 r8 r4 r8 | c4 r8 r4 r8 | b4.\fermata r4 g8 |
      % ------------- Takt 183
      c c c c c c | b( d g) g( f d) | c c c c b c | d g, g g4 g8 | c c c c c c | b( d g) g( f d) | e g c, d c d | c4 r8 r4 r8 | R2.*10 | r4 r8 r r c\f |
      % ------------- Takt
      c c c c c c | b( d g) g( f d) | e g c, d c d | c4.~c4 c,8 | c' c c c c c | b( d g) g( f d) | e g c, d c d | c4 r8 r4 r8 | r4 r8 r r g | g' e c g e c | g4 r8 r r g' | g' e c g e c | g4 r8 r r g' | c g' e c g e | c4 r8 c4 r8 | c4 r8 r4 r8 |

      \bar "|."
    }
  }
}
\addQuote "saxR" { \SaxR }

GeigeR = {
  \relative c'' {
    \compressFullBarRests
    \clef "treble" \key es \major \time 6/8

    \partial 8 r8 |
    es,4\p r8 es4 r8 | d4 r8 f4 r8 | es4 r8 es4 r8 | d4 r8 r4 r8 | es4 r8 es4 r8 | d4 r8 f4 r8 | es4 r8 d c d | es es es es r bes'\f | es es es es es es | d( f bes) bes( as f) | es es es es d es | f bes, bes bes4 bes8 | es es es es es es | d( f bes) bes( as f) | g bes es, f es f | es es, es es r r |
    % ------------- Takt 17
    r es\pp es es es es | r es es es es es | r c c r bes bes | r bes bes bes bes bes | r es es es es es | r es es es es es | r d d d es f | es4 g'8\mf( f4 es8) | d4( es8 f4 g8) | es4( f8 es4 d8) | c4( d8 es4 f8) | d4 r8 r4 r8 | bes2.\p( | a4) f'8 f g a | bes4( g8) d4( e8) |
    % ------------- Takt 32
    f4 r8 a,, bes a | bes4 bes8 bes4 bes8 | a4 a8 a bes a | bes4 bes8 bes4 bes8 | a g' f es f es | d es d c d c | bes4. d4. | es2. | f2.~ | f2.~ | f4 f'8\mf( d4 bes'8) | g4( es8 bes4 bes'8) | f4( d8 bes4 bes'8) | bes( a g f g a) | bes r r <f, d'>\< r r | <g es'> r r <bes f'> r r |
    % ------------- Takt 48
    <bes g'>4\f r8 r4 r8 | r g\mf g g( a bes) | bes4.~bes4 es8 | r d d r c c | bes r r <f d'>\< r r | <g es'> r r <bes f'> r r | <bes g'>4\f r8 r4 r8 | r g\pp g g( a bes) | bes d, d d r r | d d d d r r | f'(g e) f( g e) | f f f es es es | d4 r8 r4 r8 | es4. c4( a8) | f'2. | es4. c4( f8) | f4. d4( bes8) | bes'4( as8) g( as bes) | bes,2.~ | bes2. |
    % ------------- Takt 68
    es,4\p r8 es4 r8 | d4 r8 f4 r8 | es4 r8 es4 r8 | d4 r8 r4 r8 | es4 r8 es4 r8 | d4 r8 f4 r8 | es4 r8 d c d | es es es es r r | es4\mf r8 g4 r8 | f4 r8 d4 r8 | es4 r8 c4 r8 | bes4 r8 r as' f | es4 r8 g4 r8 | f4 r8 d4 r8 |  es4 r8 bes4 r8 | es8 es es es r r\fermata |
    % ------------- Takt 84
    r2. | r8 es\ppp es es es es | r f f f f f | r f( es) es es es | r c c b( g d') | r c c f( g, d') | c4. c'4. | b8( as g f es d) | es4 r8 r4 r8 | r8 es es es es es | r f f f f f | f4.~f8 es( d) | r es es r des des | r c c r ges' ges | r f( des) r c( bes) | as4 es''8\<( des4 c8) |
    % ------------- Takt 100
    bes4( c8 des4 es8) | c4( f8) f( es des) | c4( des8 es4 f8) | d4( g8) g( f es) | d4( es8 f4 g8) | es4\mf r8 r4 r8 | r r c,\pp c bes c | d c d es d es | c bes c d c d | g,4 r8 r4 r8 | r r c c bes c | d c d es d es | c bes c d c d | g,4 r8 r4 r8 | fis''2.\<( | g8) g g g g g | a( bes c) d( c a) | g4 r8 r4 r8 | e2.( | f8) f f f f f | g( as bes) c( bes g) | f4 r8 r4 r8 | d2.( | es8) es es es es es | f( g as) bes( as f) |
    % ------------- Takt 125
    es4\mf r8 es,4\p r8 | d4 r8 f4 r8 | es4 r8 es4 r8 | d4 r8 r4 r8 | es4 r8 es4 r8 | d4 r8 f4 r8 | es4 r8 d c d | es es es es r bes'\f | es es es es es es | d( f bes) bes( as f) | es es es es d es | f bes, bes bes4 bes8 | es es es es es es | d( f bes) bes( as f) | g bes es, f es f | es es, es es r r |
    % ------------- Takt 141
    r es\p es es es es | r es es es es es | r c c r bes bes | r bes bes bes bes bes | r es es es es es | es2.~ | es8 es es es es es | es2.~ | es8 es es es es es | es( ges f ges f es) | bes4 r8 r4 r8 | R2.*9 | r4 bes'8\mf( g4 es'8) | c4( as8 es4 es'8) | bes4( g8 es4 es'8) | es( d c bes c d) | es r r <bes f'>\f r r | <bes g'> r r <bes as'> r r | <bes bes'>4 r8 r4 r8 | r e,\p e e e e | r es es es es es | r d d d d d | es2.~ | es4.( d4.) | es8 r r <bes' f'>\f r r | <bes g'> r r <bes as'> r r | <bes bes'>4 r8 r4 r8 |  r e,\p e e e e | r es es es es es | r d d d d d | es4 r8 r4 r8 | ces4 r8 r4 r8 | ges'4 r8 r4 r8 | f4.\fermata  r4 r8 |
    % ------------- Takt 183
    R2.*6 | r4 r8 r4 bes8\ff | es es es es es es | d( f bes) bes( as f) | e( g c) c( bes g) | as-. c-. e,-. f-. as-. c,-. | d-. f-. a,-. bes-. f'-. as,-. | g4.:16 es'4.: | es4.: d4.: | g2.: | f2.: | es'2.: | es4.: d4.: | es2.: | R2.*5 | f,,8\f r r as r r | g r r f r r | es( g bes es, g bes) | f( as bes f as bes) | es,( g bes es, g bes) | f( as bes f as bes) | es,( g bes es, g bes) | f( as bes f as bes) | g es es es es es | es4 r8 <bes' g'>4 r8 | es,4 r8 r4 r8 |

    \bar "|."
  }
}
\addQuote "geigeR" { \GeigeR }

CelloR = {
  \relative c {
    \compressFullBarRests
    \clef "bass" \key es \major \time 6/8

    \partial 8 r8 |
    es4\p r8 g4 r8 | f4 r8 d4 r8 | es4 r8 c4 r8 | bes4 r8 r as' f | es4 r8 g4 r8 | f4 r8 d4 r8 | es4 r8 bes bes bes | es es es es r r | es4 r8 g4 r8 | f4 r8 d4 r8 | es4 r8 c4 r8 | bes4 r8 r as' f | es4 r8 g4 r8 | f4 r8 d4 r8 | es4 r8 bes4 r8 | es es es es r r |
    % ------------- Takt 17
    es4 r8 r4 r8 | es4 r8 r4 r8 | as,4.( bes4.) | es,4 r8 r4 r8 | es'4 r8 r4 r8 | es4 r8 r4 r8 | bes2.( | es4) r8 r4 r8 | g2.( | c,4) r8 r4 r8 | f2.( | bes,4) r8 r4 r8 | g'2.( | f4) r8 r4 r8 | g2.( |
    % ------------- Takt 32
    f4) f8 f g f | e4 e8 e4 e8 | f4 f8 f g f | e4 e8 e4 e8 | f g f es f es | d es d c d c | bes2.~ | bes2.~ | bes2. | f'2. | d4 d'8\mf( bes4 d8) | es4( bes8 g4 g'8) | d4( bes8 d,4 d'8) | c4( es8) a,( bes c) | d bes\< bes as bes as | g f es d c bes |
    % ------------- Takt 48
    es4\f r8 r4 r8 | es2.\mf | d4.( es4.) | f8 r r f, r r | r bes'\< bes as bes as | g f es d c bes | es4\f r8 r4 r8 | es4.\pp( e4.) | f4 r8 r4 r8 | f4 r8 r4 r8 | d\p( es cis) d( es cis) | d d d c c c | bes\< bes bes bes bes bes | a( c d) es( d c) | d d d d d d | c( es f) g( f es) | f f f f f f | es( g as) bes( as g)\! | as\> bes as g f es | d c bes as g f |
    % ------------- Takt 68
    es'4\p r8 g4 r8 | f4 r8 d4 r8 | es4 r8 c4 r8 | bes4 r8 r as' f | es4 r8 g4 r8 | f4 r8 d4 r8 | es4 r8 bes bes bes | es es es es r bes\f | es es es es es es | d( f bes) bes( as f) | es es es es d es | f bes, bes bes4 bes8 | es es es es es es | d( f bes) bes( as f) | g bes es, f es f | es es, es es r r\fermata |
    % ------------- Takt 84
    r2. | c'2.\ppp( | b2. | c2.) | c4.( f4. | es4. b4. | c4. as'4.) | g4 r8 r4 r8 | r2. |c,2.( | ces2.) | r8 bes( c d es f) | es4. g,4. | as4. c4. | des4.( es4.) | as,4 r8 r4 r8 |
    % ------------- Takt 100
    r4 r8 es'4.\< | as,4( a8) bes r r | r4 r8 f'4. | bes,4( b8) c r r | r4 r8 g'4.( | c,4)\mf r8 r4 r8 | r\pp r c c bes c | d c d es d es | c bes c d c d | g,4 r8 r4 r8 | r r c c bes c | d c d es d es | c bes c d c d | g,8 g'\< g g g g | a( bes c) d( c a) | bes4( d8 bes4 g8) | es4.( d4 c8) | bes g' g g g g | g( as bes) c( bes g) | as4( c8 as4 f8) | des4.( c4 bes8) | as f' f f f f | f( g as) bes( as f) | g4( bes8 g4 es8) | ces4.( bes4 as8) |
    % ------------- Takt 125
    g4\mf r8 g'4\p r8 | f4 r8 d4 r8 | es4 r8 c4 r8 | bes4 r8 r as' f | es4 r8 g4 r8 | f4 r8 d4 r8 | es4 r8 bes bes bes | es es es es r r | es4 r8 g4 r8 | f4 r8 d4 r8 | es4 r8 c4 r8 | bes4 r8 r as' f | es4 r8 g4 r8 | f4 r8 d4 r8 | es4 r8 bes4 r8 | es es es es r r |
    % ------------- Takt 141
    es4 r8 r4 r8 | es4 r8 r4 r8 | as,4.( bes4.) | es,4 r8 r4 r8 | es'2.~ | es8 es es es es es | ces2.~ | ces8 ces ces ces ces ces | ces2.\<~ | ces2. | bes4.\mf~ bes8\<( a bes) | c4 c8 c( d c) | bes4 bes8 bes( a bes) | c4 c8 c( d c) | bes\f c\> bes as bes as | g as g f g f | es4. es'4.\p~ | es2.~ | es2. | bes2. | es2.~ | es2.~ | es2.~ | es2. | r8 es\f es d bes d | es bes es f bes, f' | g4 r8 r4 r8 | c,2.\p( | f2. | bes,2.) | c4.( as4.)| bes2. | r8 es\f es d bes d | es bes es f bes, f' | g4 r8 r4 r8 |  c,2.\p( | f2. | bes,2.) | ces4 r8 r4 r8 | as4 r8 r4 r8 | bes4 r8 r4 r8 | bes4.\fermata r4 r8 |
    % ------------- Takt 183
    R2.*6 | r4 r8 r4 bes8\ff | es es es es es es | d( f bes) bes( as f) | e( g c) c( bes g) | as-. c-. e,-. f-. as-. c,-.^\markup {\italic simile} | d f a, bes d bes | es d es c bes c | as g as bes as bes | es d es c' bes c | as g as bes as bes | es d es c bes c | as g as bes c bes | a2.\p( | c2.) | bes2. | bes,2. | ces2.( | bes4. a4.) | as8\f r r d r r | es r r bes r r | es4 r8 r r es | d( f bes) bes( as f) | es4 r8 r r es | d( f bes) bes( as f) | es4 r8 r r es | d( f bes) bes( as f) | es es es es es es | es4 r8 es4 r8 | es4 r8 r4 r8 |

    \bar "|."
  }
}
\addQuote "celloR" { \CelloR }


SaxRCued = {
  \transpose c e {
    \relative c'' {
      \compressFullBarRests
      \transposition bes,

      \clef "treble"
      % \key c \major
      \time 6/8

      \partial 8 \quoteDuring #"saxR" { s8 }
      \quoteDuring #"saxR" { s2.*182 }
      % ------------- Takt 183
      \quoteDuring #"saxR" { s2.*16 }
      \new CueVoice { \set instrumentCueName = "Cello" }
      \cueDuring #"celloR" #DOWN {
        \quoteDuring #"saxR" { s2.*3 }
      }
      % ------------- Takt
      \quoteDuring #"saxR" { s2.*16 }

      \bar "|."
    }
  }
}

GeigeRCued = {
  \relative c'' {
    \compressFullBarRests

    \clef "treble"
    % \key es \major
    \time 6/8

    \partial 8 \quoteDuring #"geigeR" { s8 }
    \quoteDuring #"geigeR" { s2.*140 }
    % ------------- Takt 141
    \quoteDuring #"geigeR" { s2.*18 }
    \new CueVoice { \set instrumentCueName = "Sax" }
    \cueDuringWithClef #"saxR" #UP #"treble_8" {
      \quoteDuring #"geigeR" { s2.*2 s4 }
    }
    \quoteDuring #"geigeR" { s8 s4. s2.*21 }
    % ------------- Takt 183
    \quoteDuring #"geigeR" { s2.*4 }
    \new CueVoice { \set instrumentCueName = "Sax" }
    \cueDuringWithClef #"saxR" #UP #"treble_8" {
      \quoteDuring #"geigeR" { s2.*2 s4. s4 }
    }
    \quoteDuring #"geigeR" { s8 s2.*15 s4. s4 }
    \new CueVoice { \set instrumentCueName = "Sax" }
    \cueDuringWithClef #"saxR" #UP #"treble_8" {
      \quoteDuring #"geigeR" { s8 s2. }
    }
    \quoteDuring #"geigeR" { s2.*11 }
    % ------------- Takt

    \bar "|."
  }
}

CelloRCued = {
  \relative c {
    \compressFullBarRests

    \clef "bass"
    % \key es \major
    \time 6/8

    \partial 8 \quoteDuring #"celloR" { s8 }
    \quoteDuring #"celloR" { s2.*182 }
    % ------------- Takt 183
    \quoteDuring #"celloR" { s2.*4 }
    \new CueVoice { \set instrumentCueName = "Sax" }
    \cueDuringWithClef #"saxR" #UP #"treble_8" {
      \quoteDuring #"celloR" { s2.*2 s4. s4 }
    }
    \quoteDuring #"celloR" { s8 s2.*28 }

    \bar "|."
  }
}
