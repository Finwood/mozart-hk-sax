%vim: nocursorline :
% KV 412.1: Allegro

SaxA = {
  \transpose c e {
    \relative c'' {
      \compressFullBarRests
      \transposition bes,

      \clef "treble"
      \key c\major
      \time 4/4

      \partial 4 r4 |
      R1*20 | r2 r4 g |
      % ------------- Takt 22
      c4. e8 d c b c | d4. b8 g4 g | d'4. f8 e d c d | d4( e) r c8 e | g2 f8 e d c | b a a2 f'8 d | g e c c c4 d16 c d e | c4 r r2 | c4. g8 c e g e | d4 r r2 | d4. e16 fis g8 fis e d | c4 r r2 | r8 c c c c d16 e d8 c | b( d) d( g) g4.( cis,8) | d4 r r2 | r r4 d |
      % ------------- Takt 38
      d16 c b c d8 d d16 e fis g d8 d | e16 d c b a8 r e'16 d c b a8 r | d16 c b c d8 d d16 e fis g d8 d | e16 d c b a8 r e'16 d c b a8 r | r1 | g2 g' | a, e' | d c4.^\trill( b16 c) | b4 r r2 | g8 a16 b c d e fis g2 | e cis | d4. e16 fis \tuplet 3/2 {g8 fis e} \tuplet 3/2 {d c b} | \afterGrace a1^\trill {g16( a)} |
      % ------------- Takt 51
      g4 r r2 | R1*20 | r4 e'-. e-. e-. |
      % ------------- Takt 73
      e1~ | e8( f fis g f e d c) | f2 a, | bes b  c2. d8 e | f4 r r2 | R1*2 | r2 r4 a, | d4. f8 e d cis d | e4. cis8 a4 a | e'4. g8 f e d e | e4( f) r2 | R1*11 | r2 r4 g, |
      % ------------- Takt 98
      c4. e8 d c b c | d4. b8 g4 g | d'4. f8 e d c d | d4( e) r c8 e | g2 f8 e d c | b a a2 f'8 d | g e c c c4 d16 c d e | c4 r r2 | c2 b8 c d e | f4 r r2 | f4. e16 d cis8 d e cis | d4 r r2 | d2 d4. d8 | d16 e d e f g f g a8 f d c | b4 r r2 | r2 r8 g g g | g16 a b a b c d c d e f e f8 b, | c c e c g4 r8 g | g16 a b a b c d c d e f e f8 b, | c4 r r2 | R1*6 | r2 r4 r8 g |
      % ------------- Takt 125
      c8 c e16 f g e d4. e8 | f a, b c d8. b16 g8 g | c c e16 f g e d4. e8 | f a, c b b4( c8) r | r1 | r2 r4 r8 g | g16 a b c d e f d g8. e16 c8 c | g16 a b c d e f d g8. e16 c8 c | c4 cis d16 cis d e d e f e | d cis d e d e f e d cis d e d e f d | c8 e g2 f16 e d c | \afterGrace d1^\trill {c16( d)} | c4 r r2 | R1*6 |

      \bar "|."
    }
  }
}
\addQuote "saxA" { \SaxA }

GeigeA = {
  \relative c'' {
    \compressFullBarRests
    \clef "treble" \key d \major \time 4/4

    \partial 4 a4\mf |
    d4.( fis8 e d cis d) | e4.( cis8) a4 a | e'4.( g8 fis e d e) | e4( fis) r d8( fis) | a2 g8( fis e d) | cis( b) b2 g'8( e) | a8.( fis16) d8 d d4 \appoggiatura fis16 e16( d e fis) | d4 r8 a'16\f fis g e cis a g' e cis' a | d4 r8 a16 fis g e cis a g' e cis' a | d\> d cis cis b b a a g g fis fis e e d d\! | a'4 a, r a\mf | d8 d fis16( g a fis) e4.( fis8) | g b,( cis d) e8. cis16 a8 a | d d fis16( g a fis) e8.( fis16 e8 fis) | g b,( d cis) cis4( d8) r | a'16\p( a,) a'( a,) a'( a,) a'( a,) a'( a,) a'( a,) a'( a,) a'( a,) | b g' b a g fis e d cis b a g fis e d cis | d8 a''16( a,) a'( a,)a'( a,)a'( a,)a'( a,)a'( a,)a'( a,) | b e g b fis( a) g( e) d8 a'16\f fis g e a, cis' | d8 a16 fis g e a, cis' d a cis a d a cis a | d8 d,, d d d4 r4 |
    % ------------- Takt 22
    r8 d\p d d d d d d | r cis cis cis cis cis cis cis | r cis cis cis cis cis cis cis | r cis( d) d d d d d | r d d d r d d d | r d d d r d( e) e | fis r a r fis r cis r | d a''16\f fis g e a, cis' d8 a16 fis g e a, cis' | d8 d,,\p d d d d d d | cis e'16\f cis d b e, gis' a8 e16 cis d b e, gis' | a8 e,\p e e e e e e | e d'16\f gis b8 cis^\trill d d,16 gis b8 cis^\trill | d4 r d,,2\p( | cis8) cis cis cis r dis dis dis | e e16\f e gis gis b b e e gis gis b b gis gis | e8 gis e, e e4 r |
    % ------------- Takt 38
    r8 cis\p cis cis r e e e | r d d d r d d d | r cis cis cis r e e e | r d d d r d d d | a'\f b16 cis d e fis gis a8 a16 e b'8 b16 e, | cis'8 a, a a r e e e | r a a a r b, b b | r cis cis cis r gis' gis gis | a a'\f~ a16 cis b a gis b a gis fis a gis fis | e4 r r8 e,\p e e | r fis fis fis r dis dis dis | e cis' cis cis cis cis cis cis | b2:16\< b2: |
    % ------------- Takt 51
    a4\f r8 e'16 cis d b gis e d' b gis' e | a4 r8 e16 cis d b gis e d' b gis' e | a a cis cis a a eis eis fis fis a a fis fis cis cis | d8 r d16 b' d, b' cis, a' cis, a' b, gis' b, gis' | a,4 r8 e'16 cis d b gis e d' b gis' e | a4 r8 e16 cis d b gis e d' b gis' e | a a cis cis a a eis eis fis fis a a fis fis cis cis | d8 r d16 b' d, b' cis, a' cis, a' b, gis' b, gis' | a a cis cis a a eis eis fis fis a a fis fis cis cis | d8 r d16 b' d, b' cis, a' cis, a' b, gis' b, gis' | a4 r r e\p |
    % ------------- Takt 62
    a4.( cis8 b a gis a) | b4.( gis8) e4 e | b'4.( d8 cis b a b) | b4( cis) r a8( cis | e2 d8 cis b a) | a( g) r g g( fis) r e | e( d) r fis r b r eis, | g( fis) r ais r b r eis, | g( fis) r ais r b r eis, | g( fis) r fis r fis r fis | fis4 r r2 |
    % ------------- Takt 73
    r4 c'16\f a fis a c a c a c a c a | c4 r r2 | r8 b,,\p b b r d d d | r g g g r g g g | g4 r r d'\mf | g4.( b8 a g fis g) | a4.( fis8) d4 d | a'4.( c8 b a g a) | a4( b) r2 | r8 e,,\p e e e e e e | r dis dis dis dis dis dis dis | r dis dis dis dis dis dis dis | dis4( e) r8 g'\f g g | g16 a g fis g e cis a g' a g fis g e cis a | fis' g fis e fis d fis d a'4 r8 a | a16 b a gis a fis d a a' b a gis a fis d a | g' a g fis g e cis e g4 r8 g | g16 a g fis g e cis a g' a g fis g e cis a | fis'8 a16 a b b a a g g fis fis e e d d | a8 a'16 b d cis b a d8 a16 a fis fis d d | a8 a'16 b d cis b a d8 a16 a fis fis d d | a4 r r8 a16( cis e8.) e16 | e4 r r8 cis16( e g8.) g16 | g4 r8. cis16 cis 4 r8. e16 | e8\> d cis b a g fis e |
    % ------------- Takt 98
    d\p d, d d d d d d | r8 cis cis cis cis cis cis cis | r8 cis cis cis cis cis cis cis | r cis( d) d d d d d | r d d d r d d d | r d d d r d( e) e | fis r a r fis r cis r | d a''16\f fis g e a, cis' d8 a16 fis g e a, cis' d8 d,,\p d d r d d d | d d'16\f b c a d, fis' g8 d16 b c a d, fis' | g8 g,\p g g r fis fis fis | e e'16\f g b8 dis, e e16 g b8 dis, | e e,\p e e r e e e | e2.( d4) | cis8 a'16\f a cis cis e e a a cis cis e e cis cis | a8 a a, a a4 r | r8 cis,\p cis cis cis cis cis( e) | r d d d d d d d | r cis cis cis cis cis cis( e) | a'16\mf g fis g a8 a a16 b cis d a8 a | b16 a g fis e8 r b'16 a g fis e8 r | a16 g fis g a8 a a16 b cis d a8 a | b16 a g fis e8 r b'16 a g fis e8 a, | d d fis16( g a fis) e4.( fis8) | g b,( cis d) e8. cis16 a8 a | d d fis16( g a fis) e8.( fis16 e8 fis) | g b,( d cis) cis4( d8) r |
    % ------------- Takt 125
    R1*3 | r2 r4 r8 a'16\f fis | fis( g) e cis a e' a cis cis( d) a fis d fis a fis | fis( g) e cis a e' a cis cis( d) a fis d8 r | r e,\p e e r fis fis fis | r e e e r fis fis fis | r fis fis fis g4 r8 dis( | e4) r8 dis( e) g b g | r fis fis fis fis fis' fis fis | e2:16\< e2: | d4\f r8 a'16 fis g e cis a g' e cis' a | d4 r8 a16 fis g e cis a g' e cis' a | d d ais ais b b gis gis a a e e fis fis d d | g8 r b r a r cis r | d a16 fis g e a, cis' d8 a16 fis g e a, cis' | d8 a16 fis g e a, cis' d a cis a d a cis a | d8 d,, d d d4 r |

    \bar "|."
  }
}
\addQuote "geigeA" { \GeigeA }

CelloA = {
  \relative c {
    \compressFullBarRests
    \clef "bass" \key d \major \time 4/4

    \partial 4 r4 |
    r8 d\pp d d d d d d | r e e e e e e e | r e e e e e e e | r e( fis) fis fis fis fis fis | r d-. fis-. a-. r d,-. fis-. a-.^\markup {\italic simile} | r d, g b r e, g b | r a, d fis a\< fis g e | <fis a>2:16\mf <g a>2: | <fis a>2: a2: | d16\> d cis cis b b a a g g fis fis e e d d | a'8\! a, a a a4 r4 | r2 r8 a'16\mf( b cis8 d) | b g( e d) cis( e) a g | fis4 d r8 cis'4\<( d8) | b g( fis e) e4( fis8) a,\f | d d fis16( g a fis) e4.( fis8) | g b,( cis d) e8. cis16 a8 a | d d fis16( g a fis) e8.( fis16 e8 fis) | g b,( d cis) d4 r8 a' | d,4 r8 a' d, a' d, a' | d, fis a fis d4 r |
    % ------------- Takt 22
    d\p r r2 | r4 a' e cis | a r r2 | r4 d'-. a-. fis-. | d-.^\markup {\italic simile} r fis r | g r g2( | fis8) r d r a' r a, r | d4 r8 a'\mf d,4 r8 a' | d,4 r d\p r | a r8 e'\mf a4 r8 e | a4 r cis,\p r | b8 r b'\mf a gis r b a | gis4\p r e2( | a4) r fis r | e r8 b\f e gis b gis | e e e e e4 r |
    % ------------- Takt 38
    a4\p r cis r | b r e, r | a r cis r | b r e, r | a,8\f b16 cis d e fis gis a8 a16 e b'8 b16 e, | cis'4 r cis,\p r | d r dis r | e r eis r | fis8 r fis fis e e d d | cis4 r cis r | d r fis r | e8 e e e e e e e | e\< e e e e e e e |
    % ------------- Takt 51
    a,\f cis e a e gis b e | a,, cis e a e gis b e | a, cis a eis fis a fis cis | d b'16 cis d8 b e cis16 d e8 e, | a, cis e a e gis b e | a,, cis e a e gis b e | a, cis a eis fis a fis cis |d b'16 cis d8 b e cis16 d e8 e, | a cis a eis fis a fis cis |d b'16 cis d8 b e cis16 d e8 e, | a8 <e cis>\p <e cis> <e cis> <e cis>4 r4 |
    % ------------- Takt 62
    r1 | r4 e' b gis | e r r2 | r4 a e cis | a r cis'2( | b8) r b r ais r ais r | b r a r gis r g r | ais r e' r d r d r | cis r cis r b r b r | ais r b r ais r b r | ais4 r4 r2 |
    % ------------- Takt 73
    r4 d,8 d d d d d | d4 r r2 | g2( f | e es) | d1 | g,8 b b b b b b b | r c c c c c c c | r c c c c c c c | r c( b) b b( g a fis) | e4 r r2 | r4 b'' fis dis | b r r2 | r4 e e, r | a8\f cis e a a, cis e a | a, d fis a a, d fis a | a, d fis a a, d fis a | a, cis e a a, cis e a | a, cis e a a, cis e a | d, a' b a g fis e d | a a'16 b d cis b a d8  a fis d | a a'16 b d cis b a d8  a fis d | a cis16( e a8.) a16 a4 r | r8 cis,16( e a8.) a16 a4 r | r8. a16 cis8. a16 e8. a16 cis,8. e16 | a,4 r r2 |
    % ------------- Takt 98
    d4\p r r2 | r4 a' e cis | a r r2 | r4 d' a fis | d r fis r | g r g2( | fis8) r d r a' r a, r | d4 r8 a'\mf d,4 r8 a' | d,4 r c2\p( | b4) r8 d\mf g4 r8 d | g4 r a\p r | g r8 a\mf g4 r8 a | g4 r g\p r | g g,2( gis4) | a8 a\f cis e a cis e cis | a a a, a a4 r | a\p r r2 | d4 r r2 | a4 r r2 | d8 a' a a  fis a a a | e a a a a, cis cis cis | d a' a a fis a a a | e a a a a, cis cis cis | d4 r r8 a'16\mf( b cis8 d) | b g( e d) cis( e) a g | fis4 d r8 cis'4( d8) | b g( fis e) e4( fis8) r |
    % ------------- Takt 125
    r2 r8 a16( b cis8 d) | b g( e d) cis( e) a g | fis4 d r8 a'( g fis) | e g( fis e) e4( fis8) r | cis4 r8 g' fis4 r8 d | cis4 r8 g' fis4 r4 | a\p( g) fis2 | a4( g) fis2 |  b,4( a g) r8 a'( | g4) r8 a( g4) r | r a fis d | a'8\< a a a a, a a a | d\f fis a d a, cis e a | d, fis a d a, cis e a | d ais b gis a e fis d | g e16 fis g8 e a fis16 g a8 a, | d4 r8 a' d,4 r8 a' | d,4 r8 a' d, a' d, a' | d, fis a fis d4 r |

    \bar "|."
  }
}
\addQuote "celloA" { \CelloA }


SaxACued = {
  \transpose c e {
    \relative c'' {
      \compressFullBarRests
      \transposition bes,

      \clef "treble"
      % \key c \major
      \time 4/4

      \partial 4 \quoteDuring #"saxA" { s4 }
      \quoteDuring #"saxA" { s1*19 }
      \new CueVoice { \set instrumentCueName = "Geige" }
      \cueDuringWithClef #"geigeA" #UP #"treble^8" {
        \quoteDuring #"saxA" { s1 s2. }
      }
      \quoteDuring #"saxA" { s4 }
      % ------------- Takt 22
      \quoteDuring #"saxA" { s1*16 }
      % ------------- Takt 38
      \quoteDuring #"saxA" { s1*13 }
      % ------------- Takt 51
      \quoteDuring #"saxA" { s1*20 }
      \new CueVoice { \set instrumentCueName = "Geige" }
      \cueDuringWithClef #"geigeA" #UP #"treble^8" {
        \quoteDuring #"saxA" { s1 s4 }
      }
      \quoteDuring #"saxA" { s4 s2 }
      % ------------- Takt 73
      \quoteDuring #"saxA" { s1*24 }
      \new CueVoice { \set instrumentCueName = "Geige" }
      \cueDuringWithClef #"geigeA" #UP #"treble^8" {
        \quoteDuring #"saxA" { s2. }
      }
      \quoteDuring #"saxA" { s4 }
      % ------------- Takt 98
      \quoteDuring #"saxA" { s1*25 }
      \new CueVoice { \set instrumentCueName = "Geige" }
      \cueDuringWithClef #"geigeA" #UP #"treble^8" {
        \quoteDuring #"saxA" { s1 s2 s4. }
      }
      \quoteDuring #"saxA" { s8 }
      % ------------- Takt 125
      \quoteDuring #"saxA" { s1*19 }

      \bar "|."
    }
  }
}
