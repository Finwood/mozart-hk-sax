TARGETS = partitur sax geige cello


SRCFILES = 1-allegro.ly 2-larghetto.ly 3-rondo.ly paper.ly
PDFTARGETS = $(TARGETS:=.pdf)

all: $(PDFTARGETS)

%.pdf %.midi: %.ly $(SRCFILES)
	lilypond $<

clean: clean-midi
	rm -f $(PDFTARGETS)

clean-midi:
	rm -f $(TARGETS:=.midi) $(TARGETS:=-?.midi)
