\paper {
    after-title-space = 0\cm
    before-title-space = 0\cm
    between-system-space = 4\mm
    between-system-padding = 0\mm
    ragged-last-bottom = ##f
    print-first-page-number = ##f
    top-margin = 8\mm
    bottom-margin = 8\mm
    left-margin = 12\mm
    right-margin = 8\mm
}

\pointAndClickOff
