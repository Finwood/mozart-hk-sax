%vim: nocursorline :
% KV 447.2: Larghetto

SaxL = {
  \transpose as bes {
    \transpose f bes {
      \relative c'' {
        \compressFullBarRests
        \transposition bes,

        \clef "treble" \key f \major \time 2/2

        c4.\mf f8 a,4 a | bes8( c d bes) g4 r8 g | a r bes r c r d( bes) | a2 g8 a bes b | c4. f8 a,4 a | bes8( c d bes) g4 r8 c, | c( e g bes) a( c f d) | c r e r f4 r | R1*8 | g4. f8 e d c bes | bes( a) d( c) c4 r | R1*2 | g'4. f8 e d c bes | bes( a) d( c) c4 r | r1 | c16 d c d e f e f g( e) c c f( d) b b | c16 d c d e f e f g( e) c c f( d) b b | c8 c, c c c c c c | c1 | R1*8 | r1 |
        % ------------- Takt 37
        f'4. d8 b4 r8 g | g'4. e8 c4 r8 cis | d4~ d16( e d e) f8( d) f( d) | c2( b4) r | R1*4 | e,4.( g8) c,4 cis | d8( e f d) b4 r8 g | c( e) g g g( f e d) | c4  d8. c16 c8 c'-.( c-. c-.) | des1\fp | g,1\fp | c1\fp | c,1\fp | R1*3 | r8 c\p c c c2~ | c8 c' c c c2~ | c8( e g f e d c bes) |
        % ------------- Takt 59
        a4 r r2 | R1*3 | c4.\f f8 a,4 a | bes8( c d bes) g4 r8 c, | c( e g bes) a( c f d) | c r e r f4 r | R1*3 | r2 r4 r8 c, | c( e g bes) a( c f d) | c r e r f4 r | g,1 | c,2~ c4. c8 | c( e g bes) a( c f d) | c r e r f4 r | r1 | c8 r e r f4 r | c8 r c, r c4 r |

        \bar "|."
      }
    }
  }
}
\addQuote "saxL" { \SaxL }

GeigeL = {
  \transpose as bes {
    \relative c'' {
      \compressFullBarRests
      \clef "treble" \key aes \major \time 2/2

      r8 c,\p c c r c c c | r des des des r des des des | r c r des r es r des  | r c( es c) bes4 r | r8 c c c r c c c | r des des des r des des des | des2( aes8) r des r | es r es r as, as( c es) | c( es c es c es c es) | bes( es bes es des es des es) | c( as' des, as' es as f bes) | aes2( g4) r | c,8( es c es c es c es) | bes( es bes es des es des es) | des4.( bes8) c r bes r | c c des des c4 r | r8 bes bes bes r bes bes bes | r c c c r c c c | <as d> <as d>4 <as d> <as d> <as d>8 | <g es'>4 r r2 | r8 bes bes bes r bes bes bes | r c c c r c c c | <as d> <as d>4 <as d> <as d> <as d>8 | <g es'>4 r r aes'( | g) r r aes( | g) r r2 | r8 des\< des des des des des des | c\p( es c es c es c es) | bes( es bes es des es des es) | c( as' des, as' es as f bes) | aes2( g4) r | c,8( es c es c es c es) | bes( es bes es des es des es) | des4.( bes8) c r bes r | c-. c-. des-. des-. c4 r | as'8-. as-. bes-. bes-. as4 r |
      % ------------- Takt 37
      r8 d,( f d) f( d) d( bes) | r es( g es) g( es) e( c) | f4~ f16( g f g) aes8( f) aes( f) | es2( d4) r | g,8( bes g bes g bes g bes) | aes (bes aes bes aes bes aes bes) | g( bes aes d bes es c f) | es2( d4) r | r8 bes bes bes r c c c | r c c c r bes bes bes | bes2 des8( c e f) | es es aes, aes g4 r | r4 fes''2.\fp | r4 g2.\fp | r4 aes2.\fp | r4 bes2.\fp | ces2\f d,, | es4 r \appoggiatura {es'16 f} g8 r \appoggiatura {g16 aes} bes8 r | \appoggiatura {d,16} es4 r \appoggiatura {es16 f} g8 r \appoggiatura {g16 aes} bes8 r | \appoggiatura {d,16} es4 r \appoggiatura {es,16 f} g8\p r \appoggiatura {g16 aes} bes8 r | \appoggiatura {bes16 c} des4 r \appoggiatura {f16 es} des8 r \appoggiatura {des16 c} bes8 r | \appoggiatura {bes16 aes} g4 r r2 |
      % ------------- Takt 59
      c,8( es c es c es c es) | bes( es bes es des es des es) | c( as' des, as' es as f bes) | aes2( g4) r | r8 c, c c r c c c | r des des des r des des des |  des2( c8) r bes r | c( aes) bes( g) aes4 c8\<( es) | aes,4\mf( c des4. c16 aes) | g4( g') aes8 r f r | es4.( des8) c4 r | r8 es es es es4 r | r8 es-. es( g) aes r aes r | aes r des r c4 r | aes2.( f4) | bes2.( g4) | es2~ es8 r f r | aes, r des r c4 r | r des8 r c r r4 | c'8 r bes r aes r r4 | es8-. es-. des-. des-. c r r4 |

      \bar "|."
    }
  }
}
\addQuote "geigeL" { \GeigeL }

CelloL = {
  \transpose as bes {
    \relative c {
      \compressFullBarRests
      \clef "bass" \key aes \major \time 2/2

      aes4\p r aes r | g r g r | aes8 r bes r c r des r | es2~ es4 r |  aes, r aes r | g r g r | es2( aes8) r f' r | es( c) des( bes) c4 r | es4.\mf( aes8) c, r c r | des( es f des) bes4 r8 bes | c r des r es r f8.( des16) | c2( bes8\< c des d) | es4.\f( aes8) c, r c r | des( es f des) bes4 r8 es,\p | es( g bes des c es as f) | es r g r as4 r | es r es r | es r es r | aes2\mf \appoggiatura {f16 g} aes4 \appoggiatura {f16 g} aes8. d,16 | es8 es16\p( d es8) f-. g-. aes-. a( bes) | es,4 r es r | es4 r es r | d8\mf r \appoggiatura {d16 es} f8 r \appoggiatura {f16 g} aes8 r \appoggiatura {aes16 bes} c8 r | \appoggiatura {d,16} es4 r r d\p( | es) r r d( | es) r r2 | es1\< | es4.\mf( aes8) c, r c r | des( es f des) bes4 r8 bes | c r des r es r f8.( des16) | c2( bes8\< c des d) | es4.\f( aes8) c, r c r | des( es f des) bes4 r8 es,\p | es( g bes des c es as f) | es r g r as4 r | c,8 r e r f4 r |
      % ------------- Takt 37
      r8 f( aes f) aes( f) f( d) | r g( bes g) bes( g) g( e) | aes4~ aes16( bes aes bes) c8( aes) c( aes) | g2( f8 g aes a) | bes4.( es8) g, r g r | aes( bes c aes) f4 r8 f | g r aes r bes r c8.( aes16) | g2( f8 g aes a) | bes4.( es8) g, r g r | aes( bes c aes) f4 r8 d | es2 e8( f g aes) | g g d d es4 r | r8 fes,\fp fes fes fes fes fes fes | r g\fp g g g g g g | r aes\fp aes aes aes aes aes aes | r des\fp des des des des des des | r aes'\p aes aes fes fes fes fes | es es es es es es es es | es es es es es es es es | es4 r r \appoggiatura {es16 f} g8 r | \appoggiatura {g16 aes} bes4 r \appoggiatura {des16 c} bes8 r \appoggiatura {bes16 aes} g8 r | \appoggiatura {g16 f} es4 es-.\mf( es-. es-.) |
      % ------------- Takt 59
      es4.( aes8) c, r c r | des( es f des) bes4 r8 bes | c r des r es r f8.( des16) | c2( bes8\< c des d) | es4\p r aes, r | g r g r | es2( aes8) r f' r | es( c) des( bes) aes es'\<( aes bes) | c4.\f( des16 c) bes8-. g( bes c) | des4.( es16 des) c8( es) des( f) | f16( es) des c es( des) c bes aes8 es( aes bes) | c4.( des16 c) bes8-. g( bes c) | des4.( es16 des) c8( es) des( f) | f16( es) des c es( des) c bes aes8 aes( f es) | d-. aes'( f es d c bes aes) | g-. des''( bes aes g f es d) | des4.( es16 des) c8( es) des( f) | f16( es) des c es( des) c bes aes4 r | es'8 r g r aes r r4 | es8 r des r c r r4 | es8 r g, r aes r r4 |

      \bar "|."
    }
  }
}
\addQuote "celloL" { \CelloL }


SaxLCued = {
  \transpose c e {
    \relative c'' {
      \compressFullBarRests
      \transposition bes,

      \clef "treble"
      % \key f \major
      \time 2/2

      \quoteDuring #"saxL" { s1*14 }
      \quoteDuring #"saxL" { s1*6 }
      \new CueVoice { \set instrumentCueName = "Cello" }
      \cueDuring #"celloL" #DOWN {
        \quoteDuring #"saxL" { s1*2 }
      }
      \quoteDuring #"saxL" { s1*17 }
      \new CueVoice { \set instrumentCueName = "Cello" }
      \cueDuring #"celloL" #DOWN {
        \quoteDuring #"saxL" { s1*3 }
      }
      % ------------- Takt 37
      \quoteDuring #"saxL" { s1*7 }
      \new CueVoice { \set instrumentCueName = "Cello" }
      \cueDuring #"celloL" #DOWN {
        \quoteDuring #"saxL" { s1*1 }
      }
      \quoteDuring #"saxL" { s1*14 }
      % ------------- Takt 59
      \quoteDuring #"saxL" { s1*21 }

      \bar "|."
    }
  }
}
