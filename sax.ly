%vim: nocursorline :

\include "1-allegro.ly"  % KV 412.1: Allegro
\include "2-larghetto.ly"  % KV 447.2: Larghetto
\include "3-rondo.ly"  % KV 495.3: Rondo

\version "2.18.2"

\include "paper.ly"


\header {
  title = "Saxophonkonzert No. 1"
  composer = "Mozart/Maike"
  instrument = "Tenor Sax."
  tagline = ##f
}

\score {
  \header {
    piece = "Allegro."
  }

  <<
    \new Staff \with {
      midiInstrument = #"baritone sax"
    } \SaxACued
  >>

  \midi {
    \tempo 4 = 120
  }

  \layout { }
}

\score {
  \header {
    piece = "Larghetto."
  }

  <<
    \new Staff \with {
      midiInstrument = #"baritone sax"
    } \SaxLCued
  >>

  \midi {
    \tempo 4 = 80
  }

  \layout {
    \context { }
  }
}

\score {
  \header {
    piece = "Rondo."
  }

  <<
    \new Staff \with {
      midiInstrument = #"baritone sax"
    } \SaxRCued
  >>

  \midi {
    \tempo 4 = 144
  }

  \layout {
    \context { }
  }
}
