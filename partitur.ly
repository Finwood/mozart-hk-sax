%vim: nocursorline :

\include "1-allegro.ly"  % KV 412.1: Allegro
\include "2-larghetto.ly"  % KV 447.2: Larghetto
\include "3-rondo.ly"  % KV 495.3: Rondo

\version "2.18.2"

\include "paper.ly"
#(set-global-staff-size 12)

\header {
  title = "Saxophonkonzert No. 1"
  composer = "Mozart/Maike"
  tagline = ##f
}

\score {
  \header {
    piece = "Allegro."
  }

  <<
    \new Staff \with {
      instrumentName = #"Tenor Sax. "
      midiInstrument = #"baritone sax"
    } \SaxA

    \new Staff \with {
      instrumentName = #"Violine "
      midiInstrument = #"violin"
    } \GeigeA

    \new Staff \with {
      instrumentName = #"Cello "
      midiInstrument = #"cello"
    } \CelloA
  >>

  \midi {
    \tempo 4 = 120
  }

  \layout {}
}

\score {
  \header {
    piece = "Larghetto."
  }

  <<
    \new Staff \with {
      instrumentName = #"Tenor Sax. "
      midiInstrument = #"baritone sax"
    } \SaxL

    \new Staff \with {
      instrumentName = #"Violine "
      midiInstrument = #"violin"
    } \GeigeL

    \new Staff \with {
      instrumentName = #"Cello "
      midiInstrument = #"cello"
    } \CelloL
  >>

  \midi {
    \tempo 4 = 80
  }

  \layout {}
}

\score {
  \header {
    piece = "Rondo."
  }

  <<
    \new Staff \with {
      instrumentName = #"Tenor Sax. "
      midiInstrument = #"baritone sax"
    } \SaxR

    \new Staff \with {
      instrumentName = #"Violine "
      midiInstrument = #"violin"
    } \GeigeR

    \new Staff \with {
      instrumentName = #"Cello "
      midiInstrument = #"cello"
    } \CelloR
  >>

  \midi {
    \tempo 4 = 144
  }

  \layout {}
}
